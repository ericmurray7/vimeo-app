import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import VideoList from '../components/video_list';
import VideoPayer from '../components/video_player';
import style from '../../styles/video-display.scss';
import spinnerStyle from '../../styles/spinner.scss';

class VideoDisplay extends Component{

  renderChildren(){
    return(
      <div className="display-container">
        <VideoPayer video={this.props.videos.selectedVideo}/>
        <VideoList videoList={this.props.videos.videoList}/>
      </div>
    )
  }

  renderLoader(){
    return (
      <div className="spinner-container">
        <div className="spinner"></div>
      </div>
    )
  }

  render(){
    let {isLoading} = this.props.videos;
    if(this.props.videos.error) return <h3>{this.props.videos.error}</h3>;
    return (
      <div id="video-display">
        {isLoading ? this.renderLoader() : this.renderChildren()}
      </div>
    )
  }
}

function mapStateToPorps({videos}){
  return{
    videos
  }
}

export default connect( mapStateToPorps )( VideoDisplay );
