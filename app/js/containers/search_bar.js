import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchVideos } from '../actions/index';
import style from '../../styles/search-bar.scss';


class SearchBar extends Component{

	constructor(props){
		super(props);
		this.onInputChange = this.onInputChange.bind(this);
		this.onFormSubmit = this.onFormSubmit.bind(this);
		this.state = {term: ""};
	}

	onInputChange(event){
		this.setState({ term:event.target.value });
	}

	componentDidMount(){
		this.props.fetchVideos('vimeo');
	}

	onFormSubmit(event){
		event.preventDefault();
		this.props.fetchVideos(this.state.term);
		this.setState({ term:'' });
	}

	render(){
		return(
			<form onSubmit={this.onFormSubmit} className="input-group">
				<input
					placeholder="Search For Videos"
					value={this.state.term}
					onChange={this.onInputChange}
				/>
				<span className="input-group-btn">
					<button type="submit" className="btn">Submit</button>
				</span>
			</form>
		);
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({ fetchVideos }, dispatch );
}

export default connect(null, mapDispatchToProps)(SearchBar);
