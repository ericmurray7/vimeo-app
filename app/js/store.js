import { createStore, applyMiddleware, compose } from 'redux';
import reducers from './reducers';
import promise from 'redux-promise';
import thunk from 'redux-thunk';

const Store = createStore(
  reducers,
  compose(
    applyMiddleware(promise,thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);

export default Store;
