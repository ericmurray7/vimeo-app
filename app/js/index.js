import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Store from './store';
import App from './components/app';

function render(App) {
  ReactDOM.render(
    <Provider store={Store}>
      <App />
    </Provider>
    ,document.querySelector('#react-container'));
}

// Instead of using buggy react-hot-loader use module.hot.accept instead to achieve HRM.
if (module.hot) {
  module.hot.accept('./components/app', () => {
    // require new version of the component.
    const App = require('./components/app').default;
    // Call render for our reload.
    render(App);
  });

  // Just incase changes are made to store and need to be reloaded.
  module.hot.accept('./store', () => {
    const App = require('./components/app').default;
    render(App);
  });
}

render(App);
