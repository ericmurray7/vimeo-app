import axios from 'axios';
import * as type from './types';

const TOKEN = '46f5d39064a680f9f5003579bbfe72e7';
const ROOT_URL = `https://api.vimeo.com/videos?access_token=${TOKEN}`;

export function fetchVideos( str ){
  const url = `${ROOT_URL}&query=${str}&per_page=20&sort=date&direction=desc`;

  return (dispatch) => {
    let request = axios.get(url);
    dispatch({
      'type': type.FETCH_VIDEOS
    });
    request.then((result)=>{
      dispatch({
        type: type.FETCH_VIDEOS_SUCCESS,
        payload: result,
        loading: false
      });
    }).catch(error => {
        dispatch({
          'type': type.FETCH_VIDEOS_ERROR
        });
    });
  }
}
