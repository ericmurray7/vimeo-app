import * as type from '../actions/types';

const INIT_STATE = {videoList:[], selectedVideo:'', isLoading:false, error:''}

export default function(state=INIT_STATE, action){
	switch(action.type){
		case type.FETCH_VIDEOS :
			return {...state,  isLoading: true};
		case type.FETCH_VIDEOS_SUCCESS :
			return {videoList:action.payload.data.data, selectedVideo: action.payload.data.data[0], isLoading:false};
		case type.SELECTED_VIDEO :
			return {...state, selectedVideo: action.payload};
		case type.FETCH_VIDEOS_ERROR :
			return {...state, isLoading:false, error:"Sorry no videos were found with that search term!"};
		default:
			return state;
	}

}
