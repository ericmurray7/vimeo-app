import { combineReducers } from 'redux';
import VideoListReducer from './reducer-video-list';

const rootReducer = combineReducers({
  videos: VideoListReducer
});

export default rootReducer;
