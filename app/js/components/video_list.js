import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as type from '../actions/types';
import jump from 'jump.js';
import {easeOutExpo} from '../utils/easing';
import style from '../../styles/video-list.scss';

class VideoList extends Component{

  constructor(props){
    super(props);
    this.onClicked = this.onClicked.bind(this);
  }

  onClicked( video ){
    this.props.onVideoClicked( video );
    jump('#top', {duration: 600, easing:easeOutExpo});
  }

  renderList(){
  return(
      this.props.videoList.map((video, index) => {
        let url = video.pictures.sizes[2].link;
        return(
          <div className="img-container" key={video.uri} onClick= {() => this.onClicked(video)}>
            <div className='fadeIn animated'>
              <img src={url} alt="Photo" />
            </div>
          </div>
        )
      })
    );
  }

  renderLoader(){
    return <h3 className="loading-text">LOADING VIDEO LIST...</h3>
  }

  render(){
    return (
      <div id="video-list">
        {this.props.videoList.length > 0 ? this.renderList() : this.renderLoader()}
      </div>
    )
  }

}

function mapDispatchToProps( dispatch ){
  return {
    onVideoClicked: (video) => {
      dispatch({ type:type.SELECTED_VIDEO, payload:video })
    }
  }
}

export default connect(null, mapDispatchToProps)(VideoList);
