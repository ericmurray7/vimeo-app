import React, {Component} from 'react';
import VideoInfo from './video_info';

export default class VideoPlayer extends Component{

  constructor( props ){
    super( props );
  }

  getVideoID( url ){
    return url.substr(url.lastIndexOf('/') + 1);
  }

  renderVideoLayer(){

    let id = this.getVideoID( this.props.video.link );
    let video_url = `https://player.vimeo.com/video/${id}?autoplay=1`;

    return(
      <div className="videoWrapper">
        <iframe ref="iframe" src={video_url}
        frameBorder="0"
        width="560"
        height="349"
        />
      </div>
    )
  }

  renderLoader(){
    return <h3 className='loading-text'>LOADING VIDEO...</h3>
  }

  render(){
    return (
      <div id="video-player">
        {this.props.video ? this.renderVideoLayer() : this.renderLoader() }
        <VideoInfo user={this.props.video.user} title={this.props.video.name}/>
      </div>
    )
  }
}
