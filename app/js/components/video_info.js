import React, {Component} from 'react';
import style from '../../styles/video-info.scss';

export default class VideoInfo extends Component{

  render(){
    if(!this.props.user) return <div></div>
    return (
      <div className="video-info">
        <h3 className="title">
          <a href={this.props.user.link} className="name" target="_blank">{this.props.user.name} </a>
          <span>- {this.props.title}</span>
        </h3>
      </div>
    )
  }
}
