import React, {Component} from 'react';
import VideoDisplay from '../containers/video_display';
import SearchBar from '../containers/search_bar';
import style from '../../styles/app.scss';

export default class App extends Component{

  render(){
    return (
      <div className="root-container">
        <SearchBar />
        <VideoDisplay />
      </div>
    )
  }
}
