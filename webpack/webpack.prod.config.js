const path = require('path')
const webpack = require('webpack')

const ROOT_DIR = path.resolve(__dirname, '../app')

module.exports = {
  devtool: 'source-map',

  entry: [
    `${ROOT_DIR}/js/index`
  ],

  output: {
    path: path.resolve(__dirname, '../public'),
    filename: 'bundle.js',
    publicPath: '/public/'
  },

  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    })
  ],

  module: {
    loaders: [
      { test: /\.js?$/,
        loader: 'babel-loader',
        include: path.join(__dirname, '../app') },
      { test: /\.scss?$/,
        loader: 'style-loader!css-loader!sass-loader',
        include: path.join(__dirname, '../app', 'styles') },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        include : path.join(__dirname, '../app', 'images'),
        loader  : 'file-loader?limit=30000&name=[name].[ext]'
      },
      { test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        loader: 'file-loader'}
    ]
  }
}
