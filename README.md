# Vimeo Test App

Built in accordance to Vimeo's test for Front End Developer.
> App allows user to search for a term related to vimeo's
> collection of videos. Each search will return 20 videos
> related to the term been searched.

  - Using Vimeo's API, create an application
  - Using React/redux as a framework
  - CSS and HTML

# Features!

  - Initial load makes a request for a search term called "vimeo"
  - Use search bar to search for videos based on users search term.
  - Redux is used to separate data from Views.
  - Simple responsive behavior.

  See [Demo App](https://ericmurray7.bitbucket.io/)

### Installation
Install the dependencies and devDependencies

```sh
$ cd vimeo
$ npm install
$ npm start
```
open browser and paste http://localhost:3009/

For production environments...

```sh
$ npm run dev
```
### Todos

 - Write Tests
 - Add Animation
 - Error handling